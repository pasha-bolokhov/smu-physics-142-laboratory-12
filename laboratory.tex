%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}
\renewcommand{\theequation}{{\color{Crimson}\arabic{equation}}}
\def\AxisLen{17.0cm}
\def\SetupColor{blue}
\def\ObjectColor{Magenta}
\def\LensColor{Cyan}
\def\LensFillColor{Cyan!20!white}
\def\ObjectHeight{1.2}
\def\FocalPointRad{0.1}
\def\LensRad{8.0}
\def\LensThickness{0.6}
\def\LaserColor{WildStrawberry}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory XII  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Lenses}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics II --- Physics 142/172L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	This laboratory will explore the optical properties of convergent lenses.
	You will find the focal length of several lenses by three methods.
	Once the focal lengths are known, you will determine the refractive index
	of the glass used to make the lenses

\bigskip\noindent
	Lenses can be either convergent (positive) or divergent (negative).
	\emph{Convergent} lenses focus (converge) parallel rays entering the lens at a focal point.
	The distance from the centre of the lens to that focal point is the focal length
	of the lens.
	The focal length is dependent on the refractive index of the lens material and
	on the radius of curvature of the lens surfaces.
	\emph{Divergent} lenses will take the same parallel light rays and
	bend them \emph{away} from each other,

\begin{center}
\begin{tikzpicture}
	% Convex lens
	\begin{scope}[shift = {(0, 2.5)}]
		% lens
		\tikzmath{
			\semiangle = acos((\LensRad - \LensThickness/2.0) / \LensRad);
			\LensHeight = \LensRad * sin(\semiangle);
		}
		\path [very thick,draw=\LensColor,fill=\LensFillColor]
			(0, -\LensHeight) arc (-\semiangle:\semiangle:\LensRad)
			arc (180.0 - \semiangle:180.0 + \semiangle:\LensRad);

		% rays
		\coordinate (focus) at (\LensRad / 2.0, 0);
		\foreach \k in {2, 1, 0, -1, -2} {
			\tikzmath{
				\y = \k * 0.9;
			}
			\draw [thick, draw = \LaserColor, style = {shorten >= -0.5cm},
				decoration = {markings, mark = at position 0.3 with {\arrow{Stealth}}},
				decoration = {markings, mark = at position 0.8 with {\arrow{Stealth}}},
				postaction = {decorate}]
				(-6, \y)	--	++(6, 0)	--	(focus);	
		}
		\node at (6, 1.4)				{\color{ProcessBlue} convergent lens};
		\draw	[thin, draw = ProcessBlue, shorten >= 4mm]
			(7, -1.2)	--	++(-1.9, 0)
			node	[midway, above]	{\color{ProcessBlue} focal point}
				--	(focus);
	\end{scope}

	% Concave lens
	\begin{scope}[shift = {(0, -2.5)}]
		\def\InternalThickness{0.3}
		% lens
		\tikzmath{
			\semiangle = acos((\LensRad - \LensThickness/2.0) / \LensRad);
			\LensHeight = \LensRad * sin(\semiangle);
		}
		\path [very thick,draw=\LensColor,fill=\LensFillColor]
			(0 - \LensThickness / 2.0 - \InternalThickness / 2.0, -\LensHeight) arc (-\semiangle:\semiangle:\LensRad)
			--	++(\LensThickness + \InternalThickness, 0)
			arc (180.0 - \semiangle:180.0 + \semiangle:\LensRad)
			--	cycle;

		% rays
		\coordinate (focus) at (-\LensRad / 2.0, 0);
		\foreach \k in {2, 1, 0, -1, -2} {
			\tikzmath{
				\y = \k * 0.9;
				\th = atan(\y / (\LensRad / 2.0));
			}
			\draw [thick, draw = \LaserColor,
				decoration = {markings, mark = at position 0.5 with {\arrow{Stealth}}},
				postaction = {decorate}]
				(-6, \y)	--	++(6, 0);
			\coordinate (centre) at	(0, 0);
			\draw [shift = {(focus)}, thick, draw = \LaserColor,
				decoration = {markings, mark = at position 0.5 with {\arrow{Stealth}}},
				postaction = {decorate}]
				(\LensRad / 2.0, \y)	--	(\th:8);
		}
		\node at (6, 1.0)	{\color{ProcessBlue} divergent lens};
	\end{scope}
\end{tikzpicture}
\end{center}

\bigskip\noindent
	In this laboratory, we will be using symmetric converging lenses with focal
	lengths between \cc{ 5 } and \cc{ \SI{25}\cm },
	although if you are looking for a challenge, there are asymmetric lenses
	available --- just ask


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               P R O C E D U R E                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Procedure}}

	Choose at least two lenses to work with.
	Each lens must have a focal length of at least \cc{ \SI{2}\cm } \emph{different}
	than the other lens(es) you will be using.
	No more than one lens can have a diameter of less than \cc{ \SI{6}\cm }.
	The following methods for determining focal length can be performed
	in any order, although the dioptre method will give the quickest determination
	that the focal lengths are sufficiently different


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       F A R   O B J E C T   M E T H O D                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Far object method}

	Assemble the optical bench with a lens holder (with lens) and a screen as shown.
	Ensure centre of lens and screen are at the same height

\bigskip\noindent
	Outside, point the optical bench at some distant object with sharp angles
	and/or high contrast (the chapel works well).
	Adjust the screen to bring the projected image into the best focus.
	The thin lens equation to determine the focal length is
\ccc
\beq
\label{thin}
	\frac 1 f	~~=~~	\frac 1 {d_o}  ~+~  \frac 1 {d_i}
	\ccb\,,
\eeq
\ccb
	where \cc{ f } is the focal length,
	\cc{ d_o } is the distance from the lens to the object,
	and \cc{ d_i } is the distance from the lens to the image.
	Because the distance to the object is very large relative to the distance to the image,
	light rays from the object can be considered to be parallel,
	and the distance from the lens to the image is the same as the focal length,
\ccc
\[
	\frac 1 f	~~=~~	\frac1 {d_i}
	\ccb\qquad\qquad\text{or}\qquad\qquad\ccc
	f		~~=~~	d_i
	\ccb\,,
\]
\ccb
	as the limit of \cc{ 1 / d_o } as \cc{ d_o } approaches infinity is \cc{ 0 }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      N E A R   O B J E C T   M E T H O D                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Near object method}

	Assemble the optical bench with a light source, object, lens holder (with lens)
	and screen as shown.
	Ensure centre of light, object, lens and screen are at the same height

\bigskip\noindent
	Adjust the object and the screen to bring the projected image into the best focus.
	The same thin lens equation \eqref{thin} applies,
	but the distance from the lens to the object must be included,
\ccc
\[
	\frac 1 f	~~=~~	\frac 1 {d_o}  ~+~  \frac 1 {d_i}
	\ccb\,,
\]
\ccb
	\emph{Use at least two different object distances to find an average focal length
	for each lens}

\bigskip\noindent
	The magnification can be found by the equation
\ccc
\[
	M	~~=~~	\frac{h_i}{h_o}
	\ccb\,,
\]
\ccb
	where \cc{ h_i } is the height of the image,
	and \cc{ h_o } is the height of the object.
	\emph{Note that the height of an upside--down image must be taken as negative}.
	This magnification can be compared to the theoretical magnification
\ccc
\[
	M_\text{theory}	~~=~~	-\,\frac{d_i}{d_o}
\]
\ccb


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          D I O P T R E   M E T H O D                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Dioptre method}

	Place the lens in the dioptre meter and adjust the dial to bring the light pattern into the best focus.
	The dioptre reading is then taken directly from the dial.
	The dioptre value is in inverse metres (\cc{ \si{\meter^{-1} } }).
	\emph{The focal length can be found by taking the reciprocal of the dioptre reading}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             S P H E R O M E T E R                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Spherometer}

	Once the focal length is determined for each lens,
	the refractive index can be determined with the lensmaker's formula:
\ccc
\[
	\frac 1 f	~~=~~	(n \,-\, 1) \lgr \frac 1 {r_1} ~-~ \frac 1 {r_2} \rgr
	\ccb\,,
\]
\ccb
	where \cc{ f } is the focal length, \cc{ n } is the refractive index,
	\cc{ r_1 } and \cc{ r_2 } are the radii of curvature of each side of the lens.
	The negative sign reflects that the radii of the curves on each surface of the lens are in the opposite direction.
	Since all of the lenses we will be using are convex on both sides,
	one of the radii will be negative.
	Therefore, for simplicity, you can consider the equation to be
\ccc
\beq
\label{lensmaker}
	\frac 1 f	~~=~~	(n \,-\, 1) \lgr \frac 1 {r_1} ~+~ \frac 1 {r_2} \rgr
	\ccb,
\eeq
\ccb
	where both radii are positive.
	If you are using a symmetric lens, the formula becomes
\ccc
\beq
\label{lensmaker:symm}
	\frac 1 f	~~=~~	\frac 2 r\, (n \,-\, 1)
	\ccb\,.
\eeq
\ccb
	To determine refractive index \cc{ n } you will need to determine the radius of curvature
	using the spherometer.
	The spherometer can be thought of as a special purpose micrometer designed for measuring the curve
	of a spherical surface.
	The principal digit is read from the post at the top of the wheel,
	and the decimal places can be read from the top of the wheel at the front of the post.
	The spherometer is placed on the lens as shown below
	(you are looking at the top of the lens from the side)

\bigskip\noindent
	The equation to determine the radius of curvature is
\ccc
\beq
\label{radius}
	r	~~=~~	\frac h 2  ~+~  \frac {l^2} {6h}
	\ccb\,,
\eeq
\ccb
	where \cc{ h } is the height of the curve (the reading of the spherometer),
	and \cc{ l } is the average distance between the sphereometer legs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

\begin{itemize}
\item[\color{alizarin}\ding{234}]
	Determine the focal length of each of your lenses by the three methods described

\item[\color{alizarin}\ding{234}]
	Compare the observed magnification in the ``near object method'' to the theoretical magnification

\item[\color{alizarin}\ding{234}]
	Use the average of the three focal lengths, the radius you determined using the spherometer
	and the lensmaker's equation to determine the refractive index of the lens material.
	Compare that to the value given in your textbook for lens glass
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
